from flask import Flask, jsonify
import time
from random import randint

app = Flask( __name__ )

liquidCount=0
 
@app .route( "/getHumidity" ,methods=['GET'] )
def index():
    num=str(randint(20,90))
    return num

@app .route( "/getTemperature" ,methods=['GET'] )
def temp():
    num=str(randint(20,50))
    return str(num)

@app .route( "/getLiquidLevel" ,methods=['GET'] )
def liquidFakeValue():
	global liquidCount
	if liquidCount == 100:
		liquidCount=0
	else:
    		liquidCount=liquidCount+1
    	return str(liquidCount)


 
if __name__ == "__main__" :
    app.run( host = '0.0.0.0' , port=8080 ,debug = False )
